from wsgiref.util import FileWrapper

from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.cache import cache
import psutil
import os
import subprocess
from django.http import StreamingHttpResponse
import cv2
import base64

def is_video(nom_file):
    extensions = ['mp4','3gp','ts','mkv','avi','flv']
    try:
        tabl = nom_file.split('.')
        for extension in extensions:
            if extension == tabl[len(tabl)-1]:
                return True
    except:
        print('error')
    return False
def get_vignette(directory):
    capture = cv2.VideoCapture(directory)
    if not capture.isOpened():
        print("erreur lors de l'ouverture de video")
        return get_image_dir()
    ret, frame = capture.read()
    if not ret:
        print("Erreur de l'ouverture du video")
        return get_image_dir()
    _, buffer = cv2.imencode('.jpg',frame)

    base64_image = base64.b64encode(buffer).decode('utf-8')
    capture.release()
    # print(base64_image)
    return base64_image

def get_image_dir():
    dossier = '/home/lucio/Images/capture.png'
    with open(dossier,'rb') as image_file:
        image_data = image_file.read()
    return base64.b64encode(image_data).decode('utf-8')

def get_data(direcotry):
    files = os.listdir(direcotry)
    liste_file = []
    print(files)
    for file in files:
        complet = os.path.join(direcotry, file)
        if (file[0] != '.'):
            if (os.path.isdir(complet)):
                fiche = {
                    "nom": file,
                    "type": "dossier",
                    "path": complet,
                    "vignette": get_image_dir()
                }
                liste_file.append(fiche)
            if is_video(file) == True :
                fiche = {
                    "nom": file,
                    "type": "fichier",
                    "path": complet,
                    "vignette": get_vignette(complet)
                }
                liste_file.append(fiche)

    return liste_file
def verrifier():
    if cache.get('fichier') == None:
        print('fdsalfj')
        cache.set('fichier','/home/lucio')

@api_view(['GET'])
def testeSary(request):
    racine = '/home/lucio/prototype'
    return Response(get_data(racine))

@api_view(['GET'])
def restor(request):
    cache.set('fichier', '/home/lucio')
    return Response('rectorer')

@api_view(['GET'])
def get_partition(request):
    liste = psutil.disk_partitions()
    return Response(liste)

@api_view(['GET'])
def get_content_defautl(request):
    verrifier()
    cache.set('fichier','/home/lucio')
    liste = get_data('/home/lucio')
    return Response(liste)

@api_view(['GET'])
def retour(request):
    verrifier()
    racine = cache.get('fichier')
    liste = racine.split('/')
    if(len(liste)==0):
        cache.set('fichier', '/')
        return Response(get_data('/'))

    newliste = []
    for i in range(0,len(liste)-1):
        newliste.append(liste[i])
    newpath = ''
    for list in newliste:
        newpath = newpath+'/'+list
    newpath1 = ''
    for i in range(1,len(newpath)):
        newpath1 = newpath1+newpath[i]
    cache.set('fichier',newpath1)
    return Response(get_data(newpath1))

@api_view(['GET'])
def get_content(request,dir):
    verrifier()
    racine = cache.get('fichier')+'/'+dir
    cache.set('fichier',racine)
    return Response(get_data(racine))

@api_view(['GET'])
def get_file_disque(request,disk):
    verrifier()
    if disk == None :
        disk = '/home/lucio'
    cache.set('fichier',disk+'/')
    print(cache.get('fichier'))
    file = os.listdir(cache.get('fichier'))
    print(file)
    return Response(file)


@api_view(['GET'])
def streamVider(request,fichier):

    # racine = cache.get('fichier')
    # racine = racine + '/' + fichier
    racine = '/home/lucio/prototype/video.mp4'
    chunk_size = 8102

    video_file = open(racine,'rb')
    response = HttpResponse(FileWrapper(video_file),content_type='video/mp4')
    response['Content-length'] = os.path.getsize(racine)
    return response

@api_view(['GET'])
def video_stream(request,fichier):
    lien = cache.get('fichier') +'/'+fichier
    def generate():
        ffmpeg_command = [
            'ffmpeg',
            '-re',  # Read the input at the original rate
            '-i', lien ,
            '-c:v', 'libx264',
            '-preset', 'ultrafast',
            '-tune', 'zerolatency',
            # '-an',  # No audio
            '-c:a','aac',
            '-f', 'flv',
            'pipe:1',  # Output to pipe
        ]

        process = subprocess.Popen(
            ffmpeg_command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=10 ** 8
        )
        while True:
            data = process.stdout.read(1024)
            if not data:
                break
            yield data
        process.stdout.close()

    response = StreamingHttpResponse(
        streaming_content=generate(),
        content_type='video/x-flv',
    )
    return response



@api_view(['GET'])
def stream_video(request):
    def generate_frames():
        cap = cv2.VideoCapture('/home/lucio/prototype/video.mp4')
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            _, buffer = cv2.imencode('.jpg', frame)
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + buffer.tobytes() + b'\r\n')

    response = StreamingHttpResponse(generate_frames(), content_type='multipart/x-mixed-replace; boundary=frame')
    return response

@api_view(['GET'])
def stream_video_with_audio(request,fichier):
    # video_path = '/home/lucio/prototype/video.mp4'
    verrifier()
    video_path = cache.get('fichier')+'/'+fichier
    def generate_frames():
        with open(video_path, 'rb') as video_file:
            while True:
                chunk = video_file.read(8192)
                if not chunk:
                    break
                yield chunk

    response = StreamingHttpResponse(generate_frames(), content_type='video/mp4')
    return response

