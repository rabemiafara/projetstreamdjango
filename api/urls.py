from django.urls import path

from api import views

# Create your views here.

urlpatterns = [
    path('/partition',views.get_partition)
]
