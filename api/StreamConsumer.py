import json
from channels.generic.websocket import AsyncWebsocketConsumer

class StreamConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def send_stream_data(self, event):
        # Renvoyer les données de streaming au client
        await self.send(text_data=json.dumps(event["data"]))
