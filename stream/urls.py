"""
URL configuration for stream project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import api
import api.views as mapviews
urlpatterns = [
    path('admin/', admin.site.urls),
    path('disk/', mapviews.get_partition),
    path('file_default/',mapviews.get_content_defautl),
    path('file/<str:dir>/',mapviews.get_content),
    path('restor/',mapviews.restor),
    path('back/',mapviews.retour),
    path('stream/<str:fichier>',mapviews.video_stream),
    path('video/<str:fichier>',mapviews.stream_video_with_audio ),
    path('teste/',mapviews.testeSary)
]
