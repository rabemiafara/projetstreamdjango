from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from api.StreamConsumer import StreamConsumer

application = ProtocolTypeRouter({
    "websocket": URLRouter([
        path("ws/stream/", StreamConsumer.as_asgi()),
    ]),
})
